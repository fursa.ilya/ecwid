package ecwid.com.fursa.ecwidtest.util;


public interface IProductCallback {

    interface IProductAddListener {

        void onProductListChanged();

        void onProductListChangeCanceled();
    }

    interface IProductEditListener {

        void showProductEditDialog(String title);
    }
}
