package ecwid.com.fursa.ecwidtest.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ecwid.com.fursa.ecwidtest.R;

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_SCREEN_DELAY_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Thread(() -> {
            try {
                Thread.sleep(SPLASH_SCREEN_DELAY_TIME);
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
