package ecwid.com.fursa.ecwidtest.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ecwid.com.fursa.ecwidtest.R;
import ecwid.com.fursa.ecwidtest.db.DBHelper;
import ecwid.com.fursa.ecwidtest.ui.fragment.dialog.ProductActionDialogFragment;
import ecwid.com.fursa.ecwidtest.ui.fragment.dialog.ProductAddDialogFragment;
import ecwid.com.fursa.ecwidtest.ui.recycler.adapter.ProductAdapter;
import ecwid.com.fursa.ecwidtest.util.IProductCallback;
import ecwid.com.fursa.ecwidtest.util.ProdConst;

public class MainActivity extends AppCompatActivity implements IProductCallback.IProductAddListener, IProductCallback.IProductEditListener, View.OnClickListener {
    private static final String LOG_TAG = ProdConst.LOG_TAG + " " + MainActivity.class.getSimpleName();

    private ProductAdapter productAdapter;
    private RecyclerView rvProducts;
    private FloatingActionButton fab;
    private LinearLayoutManager layoutManager;

    private DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = DBHelper.getInstance(getBaseContext());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        rvProducts = (RecyclerView) findViewById(R.id.rvProducts);

        productAdapter = new ProductAdapter(dbHelper.selectAllProducts(), getBaseContext());
        productAdapter.setHasStableIds(true);
        layoutManager = new LinearLayoutManager(getBaseContext());
        rvProducts.setLayoutManager(layoutManager);

        rvProducts.addItemDecoration(new DividerItemDecoration(rvProducts.getContext(), layoutManager.getOrientation()));
        rvProducts.setAdapter(productAdapter);

        fab.setOnClickListener(this);
    }


    @Override
    public void onProductListChanged() {
        productAdapter = new ProductAdapter(dbHelper.selectAllProducts(), getBaseContext());
        rvProducts.setAdapter(productAdapter);
    }

    @Override
    public void onProductListChangeCanceled() {
        Toast.makeText(this, "..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        Log.d(LOG_TAG, "fab - onClick() -> ProductAddDialogFragment.newInstance()");
        ProductAddDialogFragment productAddDialogFragment = ProductAddDialogFragment.newInstance();
        productAddDialogFragment.show(getSupportFragmentManager(), productAddDialogFragment.getTag());
    }


    @Override
    public void showProductEditDialog(String title) {
        ProductActionDialogFragment productActionDialogFragment = ProductActionDialogFragment.newInstance(title);
        productActionDialogFragment.show(getSupportFragmentManager(), productActionDialogFragment.getTag());
    }
}
