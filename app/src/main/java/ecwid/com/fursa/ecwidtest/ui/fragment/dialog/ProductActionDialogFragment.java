package ecwid.com.fursa.ecwidtest.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import ecwid.com.fursa.ecwidtest.R;
import ecwid.com.fursa.ecwidtest.db.DBHelper;
import ecwid.com.fursa.ecwidtest.db.model.Product;
import ecwid.com.fursa.ecwidtest.ui.activity.MainActivity;

public class ProductActionDialogFragment extends DialogFragment {
    private static final String PRODUCT_KEY = "product_key";
    private DBHelper db = DBHelper.getInstance(getActivity());

    public static ProductActionDialogFragment newInstance(String title) {
        ProductActionDialogFragment productActionDialogFragment = new ProductActionDialogFragment();
        Bundle args = new Bundle();
        args.putString(PRODUCT_KEY, title);
        productActionDialogFragment.setArguments(args);

        return productActionDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.user_action)
                .setItems(R.array.user_actions, (dialog, which) -> {
                   switch (which) {
                       case 0:
                           Toast.makeText(getActivity(), "Edit", Toast.LENGTH_SHORT).show();
                           EditDialogFragment editDialogFragment = EditDialogFragment.newInstance(getArguments().getString(PRODUCT_KEY));
                           editDialogFragment.show(getFragmentManager(), editDialogFragment.getTag());
                           break;
                       case 1:
                           Toast.makeText(getActivity(), "Remove", Toast.LENGTH_SHORT).show();
                           Product product = db.getProductByTitle(getArguments().getString(PRODUCT_KEY));
                           db.deleteProduct(product.getProductId(), product.getProductTitle());
                           ((MainActivity)getActivity()).onProductListChanged();
                           break;
                   }
                });
        return builder.create();

    }
}
