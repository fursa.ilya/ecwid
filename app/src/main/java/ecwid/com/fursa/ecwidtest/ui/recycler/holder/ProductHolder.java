package ecwid.com.fursa.ecwidtest.ui.recycler.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ecwid.com.fursa.ecwidtest.ui.activity.MainActivity;
import ecwid.com.fursa.ecwidtest.util.ProdConst;

public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String LOG_TAG = ProdConst.LOG_TAG + " " + ProductHolder.class.getSimpleName();
    public TextView tvProductTitle;
    private Context context;

    public ProductHolder(View itemView, TextView tvProductTitle, Context context) {
        super(itemView);

        this.tvProductTitle = tvProductTitle;
        this.context = context;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.d(LOG_TAG, "Clicked -> Title = " + tvProductTitle.getText().toString());
        ((MainActivity) view.getContext()).showProductEditDialog(tvProductTitle.getText().toString());

    }
}
