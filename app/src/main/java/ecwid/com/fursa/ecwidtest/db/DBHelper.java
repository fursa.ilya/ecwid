package ecwid.com.fursa.ecwidtest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ecwid.com.fursa.ecwidtest.db.model.Product;
import ecwid.com.fursa.ecwidtest.util.ProdConst;

public class DBHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = ProdConst.LOG_TAG + " " + DBHelper.class.getSimpleName();
    private static DBHelper instance = null;
    private SQLiteDatabase db;
    private Cursor cursor;
    private Product product;

    public DBHelper(Context context) {
        super(context, ProdConst.DATABASE_NAME, null, ProdConst.DATABASE_VERSION);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        initTables(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        db.execSQL("DROP TABLE " + ProdConst.DATABASE_TABLE);
        initTables(db);
    }

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }

        return instance;
    }

    private void initTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "
                + ProdConst.DATABASE_TABLE + " (" + ProdConst.PRODUCT_ID_COLUMN
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ProdConst.PRODUCT_NAME_COLUMN + " TEXT NOT NULL, "
                + ProdConst.PRODUCT_COUNT_COLUMN + " INTEGER, "
                + ProdConst.PRODUCT_PRICE_COLUMN + " INTEGER);");

        Log.d(LOG_TAG, "CREATE TABLE "
                + ProdConst.DATABASE_TABLE + " (" + ProdConst.PRODUCT_ID_COLUMN
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ProdConst.PRODUCT_NAME_COLUMN + " TEXT NOT NULL, "
                + ProdConst.PRODUCT_COUNT_COLUMN + " INTEGER, "
                + ProdConst.PRODUCT_PRICE_COLUMN + " INTEGER);");
    }

    public void addProduct(Product product) {
        ContentValues cv = new ContentValues();
        db.beginTransaction();
        cv.put(ProdConst.PRODUCT_NAME_COLUMN, product.getProductTitle());
        cv.put(ProdConst.PRODUCT_PRICE_COLUMN, product.getProductPrice());
        cv.put(ProdConst.PRODUCT_COUNT_COLUMN, product.getProductCount());
        db.insert(ProdConst.DATABASE_TABLE, null, cv);
        db.setTransactionSuccessful();
        Log.d(LOG_TAG, "setTransactionSuccessful()");

        db.endTransaction();
    }

    public void deleteProduct(int productId, String productTitle) {
        long deleteResult = db.delete(ProdConst.DATABASE_TABLE,
                ProdConst.PRODUCT_NAME_COLUMN + "='" + productTitle + "' AND " + ProdConst.PRODUCT_ID_COLUMN + " = " + productId, null);
        Log.d(LOG_TAG, "Delete result = " + deleteResult);
    }

    public List<Product> selectAllProducts() {
        List<Product> productList = new ArrayList<>();
        cursor = db.query(ProdConst.DATABASE_TABLE, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int productTitleIndex = cursor.getColumnIndex(ProdConst.PRODUCT_NAME_COLUMN);
            int priceIndex = cursor.getColumnIndex(ProdConst.PRODUCT_PRICE_COLUMN);
            int countIndex = cursor.getColumnIndex(ProdConst.PRODUCT_COUNT_COLUMN);

            do {
                product = new Product();
                product.setProductTitle(cursor.getString(productTitleIndex));
                product.setProductPrice(cursor.getInt(priceIndex));
                product.setProductCount(cursor.getInt(countIndex));
                productList.add(product);
                Log.d(LOG_TAG, product.toString());
            } while (cursor.moveToNext());
        } else {
            cursor.close();
        }

        if (productList.isEmpty()) {
            Log.d(LOG_TAG, "Product list is Empty");
        }
        return productList;
    }

    public Product getProductByTitle(String title) {
        cursor = db.rawQuery("SELECT * FROM " + ProdConst.DATABASE_TABLE + " WHERE " + ProdConst.PRODUCT_NAME_COLUMN + "='" + title + "'", null);

        if (cursor.moveToFirst()) {
            int productIdIndex = cursor.getColumnIndex(ProdConst.PRODUCT_ID_COLUMN);
            int productTitleIndex = cursor.getColumnIndex(ProdConst.PRODUCT_NAME_COLUMN);
            int priceIndex = cursor.getColumnIndex(ProdConst.PRODUCT_PRICE_COLUMN);
            int countIndex = cursor.getColumnIndex(ProdConst.PRODUCT_COUNT_COLUMN);

            do {
                product = new Product();
                product.setProductId(cursor.getInt(productIdIndex));
                product.setProductTitle(cursor.getString(productTitleIndex));
                product.setProductPrice(cursor.getInt(priceIndex));
                product.setProductCount(cursor.getInt(countIndex));
                Log.d(LOG_TAG, product.toString());
            } while (cursor.moveToNext());
        } else {
            cursor.close();
        }

        return product;
    }

    public void updateProduct(int productId, Product product) {
        ContentValues cv = new ContentValues();
        cv.put(ProdConst.PRODUCT_ID_COLUMN, productId);
        cv.put(ProdConst.PRODUCT_NAME_COLUMN, product.getProductTitle());
        cv.put(ProdConst.PRODUCT_PRICE_COLUMN, product.getProductPrice());
        cv.put(ProdConst.PRODUCT_COUNT_COLUMN, product.getProductCount());
        int updateResult = db.update(ProdConst.DATABASE_TABLE, cv, ProdConst.PRODUCT_ID_COLUMN + " = " + productId, null);
        Log.d(LOG_TAG, "Update result = " + updateResult);
    }
}
