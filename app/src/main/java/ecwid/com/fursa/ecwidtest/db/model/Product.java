package ecwid.com.fursa.ecwidtest.db.model;

import java.io.Serializable;

public class Product implements Serializable {
    private int productId;
    private String productTitle;
    private int productCount;
    private int productPrice;

    public Product() {}

    public Product(String productTitle, int productCount, int productPrice) {
        this.productTitle = productTitle;
        this.productCount = productCount;
        this.productPrice = productPrice;
    }

    public Product(int productId, String productTitle, int productCount, int productPrice) {
        this.productId = productId;
        this.productTitle = productTitle;
        this.productCount = productCount;
        this.productPrice = productPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productTitle='" + productTitle + '\'' +
                ", productCount=" + productCount +
                ", productPrice=" + productPrice +
                '}';
    }
}
