package ecwid.com.fursa.ecwidtest.util;

public class ProdConst {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "product_database";
    public static final String DATABASE_TABLE = "products";

    public static final String PRODUCT_ID_COLUMN = "product_id";
    public static final String PRODUCT_NAME_COLUMN = "product_title";
    public static final String PRODUCT_COUNT_COLUMN = "product_count";
    public static final String PRODUCT_PRICE_COLUMN = "product_price";

    public static final String LOG_TAG = "EcwidTAG";

    public static final String PRODUCT_SERIALIZABLE_KEY = "product_tag";

}
