package ecwid.com.fursa.ecwidtest.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import ecwid.com.fursa.ecwidtest.R;
import ecwid.com.fursa.ecwidtest.db.DBHelper;
import ecwid.com.fursa.ecwidtest.db.model.Product;
import ecwid.com.fursa.ecwidtest.ui.activity.MainActivity;
import ecwid.com.fursa.ecwidtest.util.IProductCallback;
import ecwid.com.fursa.ecwidtest.util.ProdConst;

public class EditDialogFragment extends DialogFragment {
    private static final String LOG_TAG = ProdConst.LOG_TAG + " " + EditDialogFragment.class.getSimpleName();

    private IProductCallback.IProductAddListener addListener;

    private TextInputLayout tilTitle;
    private TextInputLayout tilPrice;
    private TextInputLayout tilCount;

    private EditText etTitle;
    private EditText etPrice;
    private EditText etCount;

    private DBHelper db = DBHelper.getInstance(getActivity());


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addListener = (MainActivity) getActivity();
    }

    public static EditDialogFragment newInstance(String title) {
        EditDialogFragment editDialogFragment = new EditDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(ProdConst.PRODUCT_SERIALIZABLE_KEY, title);
        editDialogFragment.setArguments(args);
        return editDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String title = getArguments().getString(ProdConst.PRODUCT_SERIALIZABLE_KEY);
        Product product = db.getProductByTitle(title);

        View view = getActivity().getLayoutInflater().inflate(R.layout.edit_dialog, null);

        tilTitle = view.findViewById(R.id.tilEditTitle);
        etTitle = tilTitle.getEditText();

        tilPrice = view.findViewById(R.id.tilEditPrice);
        etPrice = tilPrice.getEditText();

        tilCount = view.findViewById(R.id.tilEditCount);
        etCount = tilCount.getEditText();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle(R.string.edit_product_title);
        builder.setPositiveButton(R.string.edit_btn, (dialogInterface, i) -> {

            Product newProduct = new Product();

            newProduct.setProductTitle(etTitle.getText().toString());
            newProduct.setProductCount(Integer.parseInt(etCount.getText().toString()));
            newProduct.setProductPrice(Integer.parseInt(etPrice.getText().toString()));

            db.updateProduct(product.getProductId(), newProduct);
            addListener.onProductListChanged();
        });

        builder.setNegativeButton(R.string.cancel_btn, (dialogInterface, i) -> {
           dismiss();
        });




        Log.d(LOG_TAG, "Selected = " + product.getProductTitle());

        etTitle.setText(product.getProductTitle());
        etPrice.setText(String.valueOf(product.getProductPrice()));
        etCount.setText(String.valueOf(product.getProductCount()));

        return builder.create();
    }
}
