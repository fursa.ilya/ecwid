package ecwid.com.fursa.ecwidtest.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;

import ecwid.com.fursa.ecwidtest.R;
import ecwid.com.fursa.ecwidtest.db.DBHelper;
import ecwid.com.fursa.ecwidtest.db.model.Product;
import ecwid.com.fursa.ecwidtest.ui.activity.MainActivity;
import ecwid.com.fursa.ecwidtest.util.IProductCallback;

public class ProductAddDialogFragment extends DialogFragment {
    private TextInputLayout tilTitle;
    private TextInputLayout tilPrice;
    private TextInputLayout tilCount;

    private EditText etTitle;
    private EditText etPrice;
    private EditText etCount;

    private DBHelper db = DBHelper.getInstance(getActivity());

    private IProductCallback.IProductAddListener addListener;

    public static ProductAddDialogFragment newInstance() {
        return new ProductAddDialogFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        addListener = (MainActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.product_adding_dialog, null);

        tilTitle = view.findViewById(R.id.tilEditTitle);
        etTitle = tilTitle.getEditText();

        tilPrice = view.findViewById(R.id.tilPrice);
        etPrice = tilPrice.getEditText();

        tilCount = view.findViewById(R.id.tilCount);
        etCount = tilCount.getEditText();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle(R.string.new_product_title);

        builder.setPositiveButton(R.string.ok_btn, (dialogInterface, i) -> {
            Product product = new Product();

            product.setProductTitle(etTitle.getText().toString());
            product.setProductPrice(Integer.parseInt(etPrice.getText().toString()));
            product.setProductCount(Integer.parseInt(etCount.getText().toString()));

            db.addProduct(product);
            addListener.onProductListChanged();
        });

        builder.setNegativeButton(R.string.cancel_btn, (dialogInterface, i) -> {
            addListener.onProductListChangeCanceled();
            dismiss();
        });

        return builder.create();
    }
}
