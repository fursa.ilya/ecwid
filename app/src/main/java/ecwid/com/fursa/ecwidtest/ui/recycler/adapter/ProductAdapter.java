package ecwid.com.fursa.ecwidtest.ui.recycler.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ecwid.com.fursa.ecwidtest.R;
import ecwid.com.fursa.ecwidtest.db.model.Product;
import ecwid.com.fursa.ecwidtest.ui.recycler.holder.ProductHolder;

public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {
    private List<Product> productList;
    private Context context;

    public ProductAdapter(List<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product, parent, false);
        TextView tvProductName = view.findViewById(R.id.tvProductTitle);
        return new ProductHolder(view, tvProductName, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        Product product = productList.get(position);
        holder.tvProductTitle.setText(product.getProductTitle());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}